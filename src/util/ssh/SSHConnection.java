package util.ssh;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UIKeyboardInteractive;
import com.jcraft.jsch.UserInfo;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 * The SSHConnection is used in order to make SSH connections with remote devices. In can be used to execute shell
 * commands and send/receive files to/from the remote device.
 * @author Panos Katsikogiannis
 *
 */
public class SSHConnection {

	private final String IP;
	private final String username;
	private String password;
	private boolean isConnected = false;
	private Session session = null;
	private static final Object LOCK = new Object();

	/**
	 * Constructor of the {@link SSHConnection} class.
	 * @param IP - the IP address of the remote device to connect with.
	 * @param username - the username used for the connection.
	 */
	public SSHConnection(String IP, String username) {
		this.IP = IP;
		this.username = username;
		password = null;
	}

	/**
	 * Tries to connect to the remote device. A {@link MyUserInfo} object is used in order to interact with the user (e.g. asks for password).
	 * @throws IOException in case an I/O error occurs.
	 */
	public synchronized void connect() throws IOException {
		if (isConnected) return;

		synchronized (LOCK) {
			MyUserInfo ui = null;
			try {
				JSch jsch = new JSch();
				session = jsch.getSession(username, IP, 22);
				// If a password is already provided, set it in the session. If it is null, MyUserInfo object will ask for it
				session.setPassword(password);
				ui = new MyUserInfo();
				ui.setPassword(password);
				session.setUserInfo(ui);
				// Try to connect (3 seconds timeout)
				session.connect(3000);
				// If connection was successful, the MyUserInfo object holds the password, so get it from there
				password = ui.getPassword();
			} catch (JSchException e) {
				if (ui != null && ui.getPassword() == null) throw new IOException("Login aborted by user");
				else throw new IOException("Could not connect to system " + IP + ": " + e.getMessage());
			}
		}
	}

	/**
	 * Sets the password field to the specific value.
	 * @param password - the password value to be set.
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Returns the password field.
	 * @return the password field.
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Closes the SSH connection.
	 */
	public void close() {
		synchronized (LOCK) {
			if (session != null) {
				session.disconnect();
				isConnected = false;
			}
		}
	}

	/**
	 * Returns true if SSH connection is active, false otherwise.
	 * @return true if SSH connection is active, false otherwise.
	 */
	public boolean isConnected() {
		return isConnected;
	}

	/**
	 * Returns the IP address of the remote device.
	 * @return the IP address of the remote device.
	 */
	public String getIP() {
		return IP;
	}

	/**
	 * Executes a command in a shell on the remote device. The output of the command execution is returned.
	 * @param command - the command to be executed.
	 * @return the output of the command execution.
	 * @throws IOException in case of an I/O error
	 */
	public String executeCommand(String command) throws IOException {

		StringBuffer sb = new StringBuffer();
		synchronized (LOCK) {
			if (session == null) throw new IOException("Not connected to " + IP);
			try {
				Channel channel = session.openChannel("exec");
				((ChannelExec)channel).setCommand(command);
				channel.setInputStream(null);
				((ChannelExec)channel).setErrStream(System.err);
				InputStream in = channel.getInputStream();
				channel.connect();
				BufferedReader d = new BufferedReader(new InputStreamReader(in));
				String line = null;
				while ((line = d.readLine()) != null) {
					sb.append(line + "\n");
				}
				d.close();
				in.close();
				channel.disconnect();
			} catch (JSchException e) {
				throw new IOException("Could not open channel for IP " + IP + ": " + e.getMessage());
			}
		}
		return sb.toString();
	}

	/**
	 * Executes a command in a shell on the remote device. This is a "fire and forget" execution since
	 * any output generated from the command execution is ignored.
	 * @param command - the command to be executed.
	 * @throws IOException in case of an I/O error
	 */
	public void executeCommandWithoutOutput(String command) throws IOException {
		synchronized (LOCK) {
			if (session == null) throw new IOException("Not connected to " + IP);
			try {
				Channel channel = session.openChannel("exec");
				((ChannelExec)channel).setCommand(command);
				channel.setInputStream(null);
				((ChannelExec)channel).setErrStream(System.err);
				channel.connect();
				channel.disconnect();
			} catch (JSchException e) {
				throw new IOException("Could not open channel for IP " + IP + ": " + e.getMessage());
			}
		}
	}

	/**
	 * MyUserInfo class is a help object used for interaction with the user, 
	 * for example to provide the password field for the SSH connection.
	 * @author Panos Katsikogiannis
	 *
	 */
	private static class MyUserInfo implements UserInfo, UIKeyboardInteractive {

		String passwd = null;

		/**
		 * Returns the password field.
		 * @return the password field.
		 */
		public String getPassword() {
			return passwd;
		}

		/**
		 * Sets the password field to the specific value.
		 * @param passwd - the password value to be set.
		 */
		public void setPassword(String passwd) {
			this.passwd = passwd;
		}

		/**
		 * Called when the remote device needs a Yes/No answer for a specific message. By default the method returns true (Yes) in case
		 * the message contains "RSA key fingerprint is" phrase, otherwise a dialog is presented to the user.
		 * @param str - the message provided by the remote device to be answered with Yes/No.
		 * @return true if user answered Yes, false otherwise.
		 */
		public boolean promptYesNo(String str) {
			if (str.contains("RSA key fingerprint is")) {
				return true;
			} else {
				Alert alert = new Alert(AlertType.CONFIRMATION);
				alert.setTitle("Confirmation Dialog");
				alert.setHeaderText(null);
				alert.setContentText(str);
				Optional<ButtonType> result = alert.showAndWait();
				if (result.get() == ButtonType.OK) return true;
				else return false;
			}
		}

		/**
		 * No pass-phrase is used in the SSH connections, so this method always return null.
		 * @return null
		 */
		public String getPassphrase() { 
			return null; 
		}

		/**
		 * Called when the remote device asks for the pass-phrase. Always returns true, since no pass-phrase
		 * verification is used for the SSH connection with the remote devices.
		 * @param message - a message to be displayed to the user (not used).
		 * @return true
		 */
		public boolean promptPassphrase(String message) {
			return true;
		}

		/**
		 * Called when the remote device asks for the password. If the password is already set, this method returns true
		 * without bothering the user, otherwise a dialog is displayed in order to ask for a password. If a password is
		 * provided, it is saved in the {@code passwd} field of the {@link MyUserInfo}  object.
		 * @param message - a message to be displayed to the user (not used).
		 * @return true if user provided a password (or password was already set), false otherwise.
		 */
		public boolean promptPassword(String message) {

			if (passwd != null) return true;
			else {
				Dialog<Boolean> dialog = new Dialog<>();
				dialog.setTitle("Password Dialog");
				dialog.setHeaderText(null);
				dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
				PasswordField password = new PasswordField();
				password.setPromptText("Password");
				Node loginButton = dialog.getDialogPane().lookupButton(ButtonType.OK);
				loginButton.setDisable(true);
				password.textProperty().addListener((observable, oldValue, newValue) -> {
					loginButton.setDisable(newValue.trim().isEmpty());
				});
				dialog.getDialogPane().setContent(password);
				Platform.runLater(() -> password.requestFocus());
				dialog.setResultConverter(dialogButton -> {
					if (dialogButton == ButtonType.OK) {
						passwd = password.getText();
						return true;
					}
					return false;
				});
				Optional<Boolean> result = dialog.showAndWait();
				return result.get();
			}
		}

		/**
		 * Displays a message to the user coming from the remote device.
		 * @param message - the message to be displayed.
		 */
		public void showMessage(String message) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Information");
			alert.setHeaderText(null);
			alert.setContentText(message);
			alert.showAndWait();
		}

		/**
		 * Provides a way to prompt the user for keyboard-interactive authentication. An array of the prompt messages to be
		 * answered by the user are provided together with information if they are password related. All answers are returned
		 * in a String array.
		 * @param destination - identifies the user/host pair where we want to login. (This was not sent by the remote side).
		 * @param name - the name of the request. This may be empty.
		 * @param instruction - an instruction string to be shown to the user. This may be empty, and may contain new-lines.
		 * @param prompt - an array of prompt messages coming from the remote device.
		 * @param echo - for each prompt string, whether to show the texts typed in (true) or to mask them (false). This array will have the same length as prompt.
		 * @return the answers as given by the user. This is an array of same length as prompt, if the user confirmed. If the user cancels the input, the return value is null.
		 */
		public String[] promptKeyboardInteractive(String destination, String name, String instruction, String[] prompt, boolean[] echo) {

			// If password already known, use this one and don't ask the user
			if ( passwd != null) {
				String[] response = new String[prompt.length];
				for (int i = 0; i < prompt.length; i++) {
					response[i] = "";
					if (prompt[i].contains("Password")) response[i] = passwd;
				}
				return response;
			}
			Dialog<String[]> dialog = new Dialog<>();
			dialog.setTitle("Keyboard Interactive Dialog");
			dialog.setHeaderText(instruction);
			dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
			TextField[] texts = new TextField[prompt.length];
			GridPane grid = new GridPane();
			grid.setHgap(10);
			grid.setVgap(10);
			grid.setPadding(new Insets(10, 10, 10, 10));
			for (int i = 0; i < prompt.length; i++) {
				grid.add(new Label(prompt[i]), 0, i);
				if (echo[i]) {
					texts[i] = new TextField();
				} else {
					texts[i] = new PasswordField();
				}
				grid.add(texts[i], 1, i);
			}
			dialog.getDialogPane().setContent(grid);
			dialog.setResultConverter(dialogButton -> {
				if (dialogButton == ButtonType.OK) {
					String[] response = new String[prompt.length];
					for (int i = 0; i < prompt.length; i++) {
						response[i] = texts[i].getText();
						if (prompt[i].contains("Password")) passwd = response[i];
					}
					return response;
				}
				return null;
			});
			Optional<String[]> result = dialog.showAndWait();
			return result.get();
		}
	}

	/**
	 * Sends a file to the remote device.
	 * @param localFile - the name of the local file to be transfered (whole path).
	 * @param remoteFileName - the name of the file when transferred to the remote device (file name only).
	 * @param remoteTargetDirectory - the name of the directory of the remote device to put the file.
	 * @param mode - the permissions mode of the file.
	 * @throws IOException in case an I/O error occurs.
	 */
	public void sendFile(String localFile, String remoteFileName, String remoteTargetDirectory, String mode) throws IOException {
		synchronized (LOCK) {
			SCPClient uploadClient = new SCPClient(session);
			uploadClient.put(localFile, remoteFileName, remoteTargetDirectory, mode);
		}
	}

	/**
	 * Sends multiple files to the remote device.
	 * @param localFiles - the name of the local files to be transfered (whole path).
	 * @param remoteFiles - the names of the files when transferred to the remote device (file names only).
	 * @param remoteTargetDirectory - the name of the directory of the remote device to put the files.
	 * @param mode - the permissions mode of the files.
	 * @throws IOException in case an I/O error occurs.
	 */
	public void sendFiles(String localFiles[], String remoteFiles[], String remoteTargetDirectory, String mode) throws IOException {
		synchronized (LOCK) {
			SCPClient uploadClient = new SCPClient(session);
			uploadClient.put(localFiles, remoteFiles, remoteTargetDirectory, mode);
		}
	}

	/**
	 * Retrieves a file from the remote device.
	 * @param remoteFile - the name of the file on the remote device (whole path).
	 * @param localTargetDirectory - the path to the local directory to save the retrieved file.
	 * @throws IOException in case an I/O error occurs.
	 */
	public void getFile(String remoteFile, String localTargetDirectory) throws IOException {
		synchronized (LOCK) {
			SCPClient downloadClient = new SCPClient(session);
			downloadClient.get(remoteFile, localTargetDirectory);
		}
	}

	/**
	 * Retrieves multiple files from the remote device.
	 * @param remoteFilese - the names of the files on the remote device (whole path).
	 * @param localTargetDirectory - the path to the local directory to save the retrieved files.
	 * @throws IOException in case an I/O error occurs.
	 */
	public void getFiles(String remoteFiles[], String localTargetDirectory) throws IOException {
		synchronized (LOCK) {
			SCPClient downloadClient = new SCPClient(session);
			downloadClient.get(remoteFiles, localTargetDirectory);
		}
	}
}

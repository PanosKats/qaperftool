package util.crypto;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Helper class used for encrypting and decrypting strings. 
 * @author Panos Katsikogiannis
 *
 */
public class StrongAES {

	/**
	 * Encrypts the given string using the provided key.
	 * @param text - the text to be encrypted.
	 * @param key - the key to be used for the encryption.
	 * @return a byte array with the encrypted data. 
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static byte[] encrypt(String text, byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Key aesKey = new SecretKeySpec(key, "AES");
		Cipher cipher = Cipher.getInstance("AES");

		// encrypt the text
		cipher.init(Cipher.ENCRYPT_MODE, aesKey);
		byte[] encrypted = cipher.doFinal(text.getBytes());
		
		return encrypted;
	}
	
	/**
	 * Decrypts the give byte array using the provided key.
	 * @param text - the byte array to be decrypted.
	 * @param key - the key to be used for the decryption.
	 * @return the decrypted text string.
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public static String decrypt(byte[] text, byte[] key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Key aesKey = new SecretKeySpec(key, "AES");
		Cipher cipher = Cipher.getInstance("AES");
		
		// decrypt the text
		cipher.init(Cipher.DECRYPT_MODE, aesKey);
		
		return new String(cipher.doFinal(text));
	}
	
	/**
	 * Generates a random 128 bit key.
	 * @return the generated random 128 bit key in a byte array.
	 */
	public static byte[] getRandomKey() {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[16];
		random.nextBytes(bytes);
		return bytes;
	}
}

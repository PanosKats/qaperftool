package main.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * The SystemData class hold all the data like IP address, user name, etc. for a specific system.
 * @author Panos Katsikogiannis
 *
 */
public class SystemData {

	private final StringProperty ipAddress;
	private final StringProperty userName;
	private final StringProperty encryptedPassword;
	
	/**
	 * Default constructor.
	 */
	public SystemData() {
		this(null, null);
	}
	
	/**
	 * Constructor of the {@link SystemData} class.
	 * @param ipAddress - the IP address of the system.
	 * @param userName - the user name for SSH connection of the system.
	 */
	public SystemData(String ipAddress, String userName) {
		this.ipAddress = new SimpleStringProperty(ipAddress);
		this.userName = new SimpleStringProperty(userName);
		this.encryptedPassword = new SimpleStringProperty("");
	}
	
	public String getIpAddress() {
		return ipAddress.get();
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress.set(ipAddress);
	}

	public StringProperty ipAddressProperty() {
		return ipAddress;
	}
	
	public String getUserName() {
		return userName.get();
	}
	
	public void setUserName(String userName) {
		this.userName.set(userName);
	}
	
	public StringProperty userNameProperty() {
		return userName;
	}
	
	public String getEncryptedPassword() {
		return encryptedPassword.get();
	}
	
	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword.set(encryptedPassword);
	}
	
	public StringProperty encryptedPasswordProperty() {
		return encryptedPassword;
	}
}

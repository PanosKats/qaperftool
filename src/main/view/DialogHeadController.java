package main.view;

import java.io.File;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * The DialogHeadController class is used as header for Dialogs in the application. It contains a title, an information field and
 * an image at the right side of the component. It also has a linear gradient paint from white to blue near the image (left to right). 
 * <PRE>
 * +--------------------------------------------+-------+
 * |  Title                                     | Image |
 * |    Information                             |       |
 * +--------------------------------------------+-------+
 * </PRE>
 * @author Panos Katsikogiannis
 *
 */
public class DialogHeadController {
	@FXML
	private Label titleLabel;
	@FXML
	private Label infoLabel;
	@FXML
	private ImageView imageView;

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	public void initialize() {
		titleLabel.setText("Title");
		infoLabel.setText("");
	}

	/**
	 * Sets the title of the component to the given value.
	 * @param title - the title to be set.
	 */
	public void setTitle(String title) {
		if (title != null) titleLabel.setText(title);
		else titleLabel.setText("Title");
	}

	/**
	 * Sets the information of the component to the given value.
	 * @param info - the information to be set.
	 */
	public void setInfo(String info) {
		if (info != null) infoLabel.setText(info);
		else infoLabel.setText("");
	}

	/**
	 * Sets the image URL of the component to the given value.
	 * @param imageURL - the image URL to be set.
	 */
	public void setImageURL(String imageURL) {
		if (imageURL != null) {
			File file = new File(imageURL);
			if (file.exists()) {
				Image image = new Image(file.toURI().toString());
				imageView.setImage(image);
			}
		}
	}
}

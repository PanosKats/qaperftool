package main.view;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * This class extends the {@link Stage} class and should be used to create dialogs for the application.
 * It is layered-out using a BorderPane which has by default a {@link DialogHeadController} at the top, 
 * while the main pane is added at the center of the dialog. It is important to use the {@link #setPane(Pane)}
 * method for adding the main pane to the dialog instead of creating a new Scene and calling {@link #setScene(Scene)}
 * method.
 * @author Panos Katsikogiannis
 *
 */
public class CustomStage extends Stage {

	private DialogHeadController head;
	private BorderPane rootPane;

	/**
	 * Constructor of the {@link CustomStage} class.
	 */
	public CustomStage() {
		super();
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(CustomStage.class.getResource("DialogHeadLayout.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
			head = loader.getController();
			rootPane = new BorderPane();
			rootPane.setTop(page);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sets the title in the header of the dialog to the given value.
	 * @param title - the title to be set.
	 */
	public void setInfoTitle(String title) {
		head.setTitle(title);
	}

	/**
	 * Sets the information in the header of the dialog to the given value.
	 * @param info - the information to be set.
	 */
	public void setInfo(String info) {
		head.setInfo(info);
	}

	/**
	 * Sets the image URL in the header of the dialog to the given value.
	 * @param imageURL - he image URL to be set.
	 */
	public void setImageURL(String imageURL) {
		head.setImageURL(imageURL);
	}

	/**
	 * Sets the center pane of the CustomStage to the given value. This method actually creates a new Scene 
	 * that is set as the scene of this Stage and which contains the header of the dialog at the top of it 
	 * and the given pane at the center. This method should be used instead of {@link #setScene(Scene)}.
	 * @param pane - the center pane to be set.
	 * 
	 * @see {@link Scene}, {@link CustomStage}, {@link DialogHeadController}
	 */
	public void setPane(Pane pane) {
		rootPane.setCenter(pane);
		setScene(new Scene(rootPane));
	}
}
